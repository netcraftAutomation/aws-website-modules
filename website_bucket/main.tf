resource "aws_s3_bucket" "website_bucket" {
  bucket = lower(var.name)

  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }
}